/**
 * BLOCK: section-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
import classnames from 'classnames'
import ResizableBox from 're-resizable'
import Section from './section-tag'

const { __ } = wp.i18n; // Import __() from wp.i18n
const { 
	PanelBody, 
	RangeControl,
	Button, 
	ResponsiveWrapper,
	ToggleControl,
	SelectControl,
} = wp.components;
const { Fragment } = wp.element;

const {
	InspectorControls,
	InspectorAdvancedControls,
	InnerBlocks,
	PanelColorSettings,
	MediaUpload,
} = wp.editor;

export default ( props ) => {
	const {
		attributes,
		setAttributes,
		setBackgroundColor,
		toggleSelection,
	} = props;

	const onSelectBgImage = ( media ) => {
		setAttributes( {
			bgImage: {
				id: media.id,
				image: media.sizes.large || media.sizes.full,
			}
		} )
	}

	const onRemoveBgImage = () => {
		setAttributes( {
			bgImage: null
		} )
	}

	const { tagName, customBackgroundColor, customTextColor, resizeTopIsActive, resizeBottomIsActive, bgImage, bgOptions, enablePinch, verticalAlign} = attributes

	return (
		<Fragment>
			<InspectorControls>
				<PanelBody
					title={ __( 'Sizing' ) }
					initialOpen={ true }
				>
					<ToggleControl
						label={ __( 'Pinch?' ) }
						checked={ !! attributes.enablePinch }
						onChange={ ( nextenablePinch ) => {
							setAttributes( {
								enablePinch: nextenablePinch,
							} );
						} }
					/>

					<ToggleControl
						label={ __( 'Width 50?' ) }
						checked={ !! attributes.w50 }
						onChange={ ( nextw50 ) => {
							setAttributes( {
								w50: nextw50,
							} );
						} }
					/>

					<ToggleControl
						label={ __( 'Width 75?' ) }
						checked={ !! attributes.w75 }
						onChange={ ( nextw75 ) => {
							setAttributes( {
								w75: nextw75,
							} );
						} }
					/>
					
					<ToggleControl
						label={ __( 'Vertical Align Middle?' ) }
						checked={ !! attributes.verticalAlign }
						onChange={ ( nextverticalAlign ) => {
							setAttributes( {
								verticalAlign: nextverticalAlign,
							} );
						} }
					/>
					
				</PanelBody>
				
			</InspectorControls>
			
			<section
				className={ props.className }
				style={ {
					backgroundColor: 'margin',
					color: customTextColor,
					padding: (enablePinch ? '0 10%' : '0')
				} }
				
			>
				
				
				<InnerBlocks />
				
			</section>
		</Fragment>
	);
}