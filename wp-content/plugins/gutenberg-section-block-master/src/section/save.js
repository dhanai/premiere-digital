/**
 * BLOCK: section-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
import classnames from 'classnames'
import Section from './section-tag'

const {
	InnerBlocks,
} = wp.editor;

export default ( { attributes, className } ) => {

	const { 
		enablePinch, w50, w75, verticalAlign
	} = attributes;
	const isPinched = enablePinch  ? 'is-pinched' : '';
	const isW50 = w50  ? 'w50' : '';
	const isW75 = w75  ? 'w75' : '';
	const isMiddle = verticalAlign  ? 'align-middle' : '';
	const classes = classnames(
		{
			className,
			[isPinched]: isPinched,
			[isW50]: isW50,
			[isW75]: isW75,
			[isMiddle]: isMiddle,
		}
	)

	return (
		<section className={ classes ? classes : undefined }>
			<InnerBlocks.Content />
		</section>
	);
}