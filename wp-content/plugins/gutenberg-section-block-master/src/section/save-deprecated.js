/**
 * BLOCK: section-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
import classnames from 'classnames'

const {
	InnerBlocks,
} = wp.editor;

export default ( pp ) => {
	const { attributes, className } = pp;
	const { 
		enablePinch
	} = attributes;
	const isPinched = enablePinch  ? 'is-pinched' : '';
	const classes = classnames(
		{
			className,
			[isPinched]: isPinched,
		}
	)


	return (
		<section className={ classes ? classes : undefined }>
			<InnerBlocks.Content />
		</section>
	);
}