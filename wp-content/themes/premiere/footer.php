<?php 
  global $glide_theme_options, $browser;
  $copyright = $glide_theme_options['copyright_copy']; 
  $connect = $glide_theme_options['connect_button']; 
?>

<footer>
  <div class="container">
  	<div class="row">
  		<div class="col-12">
  			<div class="wrapper">
  				<div class="left">
	  				<div class="logo">
						<a href="<?php echo esc_url( home_url( '/' )); ?>" class="d-block">
							<img src="<?php echo $glide_theme_options['primary_logo']['sizes']['large'];?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="logo-primary">
						</a>
					</div>
					<div class="footer1">
						<?php wp_nav_menu( array( 'theme_location' => 'footer1_menu' ) ); ?>
					</div>
					<div class="footer2">
						<?php wp_nav_menu( array( 'theme_location' => 'footer2_menu' ) ); ?>
					</div>
					<div class="footer3">
						<?php wp_nav_menu( array( 'theme_location' => 'footer3_menu' ) ); ?>
					</div>
				</div>
				<div class="social">
					<?php wp_nav_menu( array( 'theme_location' => 'social_menu' ) ); ?>
					<p><?=$copyright;?></p>
				</div>
  			</div>
  		</div>
  	</div>
  </div>
</footer>
<div class="pop-modal"><div class="close-modal"><div class="close-icon"></div></div><div class="details"></div></div>
<div class="modal-bg"></div>

<div class="connect-sticky d-none d-md-block"><a href="<?=$connect['url'];?>" class="btn"><?=$connect['title'];?></a></div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160812685-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-160812685-1');
</script>

<script type="text/javascript">
_linkedin_partner_id = "1903444";
window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1903444&fmt=gif" />
</noscript>

<?php wp_footer() ?>
