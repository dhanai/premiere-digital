<?php
/**
  * Header
 */

global $glide_theme_options;
global $pID;
global $glide_page_fields;
global $browser;
// 

/*
* $options_fields is all of the ACF fields from Theme Options
* $glide_page_fields is all of the ACF fields for the current page
* Putting these ACF fields into arrays saves the page from having to call the database everytime an ACF field is needed
* You can use the function snag_field() to retrieve data from the $glide_page_fields array
*/   
$glide_theme_options = get_fields('option');
$glide_page_fields = get_fields($pID);
$device_class = "";
if ($browser->is_mobile_device()) {
  $device_class = "is-mobile ";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui" />
	
  <script> 
   // Identifies the Browser type in the HTML tag for specific browser CSS
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
    doc.setAttribute("data-platform", navigator.platform );
  </script>	

  <script type="text/javascript">
piAId = '851323';
piCId = '4285';
piHostname = 'pi.pardot.com';

(function() {
    function async_load(){
        var s = document.createElement('script'); s.type = 'text/javascript';
        s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
        var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
    }
    if(window.attachEvent) { window.attachEvent('onload', async_load); }
    else { window.addEventListener('load', async_load, false); }
})();
</script>
	<?php wp_head() ?>
  
</head>

<body <?php body_class([$pID,$device_class])?>>

<div class="mobile-menu">
  <div class="nav-left">
    <div class="logo">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" class="d-block">
          <img src="<?php echo $glide_theme_options['primary_logo']['sizes']['large'];?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="logo-primary">
      </a>
    </div>
    <div class="mobile-btn">
        <a href="#"><i class="fas fa-times"></i></a>
      </div>
  </div>
  <?php wp_nav_menu( array( 'theme_location' => 'tb_left_menu' ) ); ?>
</div>
  
<header class="container-fluid">
  <nav class="row">
    <div class="nav-left">
      <div class="logo">
        <a href="<?php echo esc_url( home_url( '/' )); ?>" class="d-block">
            <img src="<?php echo $glide_theme_options['primary_logo']['sizes']['large'];?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="logo-primary">
            <img src="<?php echo $glide_theme_options['secondary_logo']['sizes']['large'];?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="logo-secondary">
        </a>
        <div class="beta d-none">beta</div>
      </div>
      <div class="nav-links d-none d-lg-flex">
        <?php wp_nav_menu( array( 'theme_location' => 'tb_left_menu' ) ); ?>
      </div>
    </div>
    <div class="nav-right">
      <div class="d-none d-lg-flex">
        <?php wp_nav_menu( array( 'theme_location' => 'tb_right_menu' ) ); ?>
      </div>
      <div class="mobile-btn d-lg-none">
        <a href="#"><i class="fas fa-bars"></i></a>
      </div>
    </div>
  </nav>  
</header>




