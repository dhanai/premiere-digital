<?php
get_header(); 

global $glide_theme_options;
global $pID;
global $glide_page_fields;

?>


  <div id="posts-container" class="container row full-list" >

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
				the_post();
				get_template_part( 'template-parts/post' );
			}
		} 
		?>

  </div>  




<?php
wp_reset_query();   
get_footer();
?>