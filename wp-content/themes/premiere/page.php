<?php
// Page

get_header(); 
$pID = get_the_ID();
the_post();

?>

<?php the_content(); ?>
<?php
get_footer();
?>