<?php 
  global $browser, $cells, $display_type, $count;
?>

<div class="cell">
  <div class="inner">
    
    <div class="post-details">
      <a href="<?php echo get_the_permalink( get_the_ID() ) ?>" class="post-title">
        <?php echo get_the_title(get_the_ID()); ?>
      </a>
    </div>
  </div>
</div>
