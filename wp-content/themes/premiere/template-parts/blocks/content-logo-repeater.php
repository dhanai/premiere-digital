<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$make_grey = $glide_page_fields['make_grey'];
	$logos = $glide_page_fields['logos'];

?>
<section class="block-logo-repeater">
    <div class="container p-0">
      <div class="row no-gutters">
        <div class="col-12 text-center">
        	<?php if($headline):?>
        		<h3><?php echo $headline;?></h3>
        	<?php endif;?>
        	<div class="logos <?php echo($make_grey ? 'grey' : '');?>">
        		<?php foreach($logos as $logo):?>
        			<div class="logo">
        				<img src="<?php echo $logo['logo']['url'];?>">
        			</div>
        		<?php endforeach;?>
        	</div>
        </div>
      </div>
    </div>
</section>