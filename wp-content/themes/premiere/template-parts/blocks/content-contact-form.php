<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$gravity_form_id = $glide_page_fields['gravity_form_id'];
	$pardot_embed = $glide_page_fields['pardot_embed'];
	$form_toggle = $glide_page_fields['form_toggle'];
	$right_content = $glide_page_fields['right_content'];

?>
<section class="block-contact-form">
	<div class="wrapper">
	    <div class="container">
	      <div class="row">
	      	<div class="col-12">
	        	<h2><?= $headline;?></h2>
	        </div>
	        <div class="col-12 col-lg-7 pr-lg-5">
	        	<div class="form-wrapper">
		        	<?php 

		        		if($form_toggle == 'gravity'){
			        		gravity_form( $gravity_form_id, false, false, true, '', false );
			        	}else{
			        		echo $pardot_embed;
			        	}

		        	?>
		        </div>
	        </div>
	        <div class="col-12 col-lg-4">
		        <p><?php echo $right_content;?></p>
	        </div>
	      </div>
	    </div>
	</div>
</section>