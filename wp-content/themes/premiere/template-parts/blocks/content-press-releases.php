<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$press_releases = $glide_page_fields['press_releases'];

?>
<section class="block-press-releases">
    <div class="container">
      <div class="row">
        <div class="col-12 pb-5">
        	<h2><?= $headline;?></h2>
        </div>
        <div class="col-12">
        	<?php foreach ($press_releases as $press):?>
        		<div class="press-release">
              <p><?php echo get_field('date', $press->ID);?></p>
	        		<h4><?php echo get_the_title( $press->ID ); ?></h4>
		        	<p><?php echo get_field('excerpt', $press->ID);?></p>
		        	<?php $link = get_field('link', $press->ID);?>
		        	<a href="<?php echo $link['url'];?>" target="<?php echo $link['target'];?>">
		        		<?php echo ($link['title'] ? $link['title'] : 'Read More' );?>
		        	</a>
		        </div>
        	<?php endforeach;?>
        </div>
      </div>
    </div>
</section>