<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$slides = $glide_page_fields['slides'];

?>
<section class="block-carousel">
    <div class="wrapper">
        <div class="container">
          <div class="row">

            <div class="col-12 swiper-container">
        		<div class="slides swiper-wrapper">
                    <?php foreach($slides as $slide):?>
                        <div class="slide swiper-slide">
                			<div class="content">
                    				<div class="label"><?php echo $slide['label'];?></div>
                    				<h2><?php echo $slide['headline'];?></h2>
                    				<a href="<?php echo $slide['cta']['url'];?>" class="btn"><?php echo $slide['cta']['title'];?></a>
                    		</div>
                    		<div class="image">
                    			<figure class="ratio ratio-4x3" style="background-image:url('<?php echo $slide['image']['sizes'][ 'large' ];?>') "></figure>
                    		</div>
                        </div>
                    <?php endforeach;?>
        		</div>
            </div>

            <div class="swiper-pagination"></div>

          </div>
        </div>
    </div>
</section>

<script>
    jQuery('document').ready(function($){

        var mySwiper = new Swiper ('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            autoplay: {
                delay: 3500,
              },
            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
              }
        });

    });
</script>