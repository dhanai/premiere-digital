<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$content = $glide_page_fields['content'];
    $show_cta = $glide_page_fields['show_cta'];
    $cta = $glide_page_fields['cta'];
	$subheader = $glide_page_fields['subheader'];
	$background_image = $glide_page_fields['background_image'];
    $blue_tint = $glide_page_fields['blue_tint'];

?>
<section class="block-hero-blue">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-7 pr-md-5">
        	<h2><?php echo $headline;?></h2>
        	<p><?php echo $content;?></p>
            <?php if($show_cta):?>
                <div class="mt-3">
                    <a href="<?=$cta['url'];?>" class="btn"><?=$cta['title'];?></a>
                </div>
            <?php endif;?>
        </div>
      </div>
    </div>
    <div class="blue-box <?php echo $blue_tint ? 'blue-tint ' : '';?>ratio ratio-4x3">
        <div class="subheader">
            <p><?php echo $subheader;?></p>
        </div>
    	<div class="blue-image" style="background-image: url('<?php echo $background_image['url'];?>');"></div>
    	<div class="blue-fill"></div>
    </div>
</section>