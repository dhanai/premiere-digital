<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
  $employees = $glide_page_fields['employees'];
	// $image = $glide_page_fields['image'];
	// $content = $glide_page_fields['content'];

?>
<section class="block-testimonial">
    <div class="container">
        <div class="row">
          <div class="col-12">
            <h3 class="d-none"><?php echo $headline;?></h3>
            <div class="swiper-container">
              <div class="swiper-wrapper">
                <?php foreach($employees as $employee): ?>
                  <div class="slide swiper-slide">
                      <div class="test-image">
                        <figure class="ratio ratio-3x4" style="background-image: url('<?php echo $employee['image']['sizes']['large'];?>');"></figure>
                      </div>
                      <div class="test-content">
                      	<?php echo $employee['content'];?>
                      </div>
                  </div>
                <?php endforeach;?>
              </div>
            </div>
            <div class="swiper-pagination"></div>
          </div>
        </div>
    </div>
</section>

<script>
    jQuery('document').ready(function($){

        var mySwiper = new Swiper ('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            // autoplay: {
            //     delay: 3500,
            //   },
            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
              }
        });

    });
</script>