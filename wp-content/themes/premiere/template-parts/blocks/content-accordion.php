<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$right_image = $glide_page_fields['right_image'];
	$accordion_items = $glide_page_fields['accordion_items'];

?>
<section class="block-accordion">
	<div class="wrapper">
	    <div class="container">
	      <div class="row">
	      	<div class="col-12">
	      		<h3><?php echo $headline;?></h3>
	        </div>
	        <div class="col-12 col-lg-7 pr-5">
	        	<ul class="accordion" data-accordion data-allow-all-closed="true">
		        	<?php foreach ($accordion_items as $item):?>
		        		<li class="accordion-item" data-accordion-item>
		        			<a href="#" class="accordion-title">
		        				<h5><strong><?=$item['headline'];?></strong></h5><i class="fas fa-plus d-none"></i>
		        			</a>
		        			<div class="accordion-content" data-tab-content>
		        				<?=$item['content'];?>
		        			</div>
		        		</li>
		        	<?php endforeach;?>
		        </ul>
	        </div>
	        <div class="col-12 col-lg-3">
	        	<div class="right-side"></div>
	        </div>
	      </div>
	    </div>
	    <div class="right-image" style="background-image: url('<?php echo $right_image['url'];?>');"></div>
	</div>
</section>