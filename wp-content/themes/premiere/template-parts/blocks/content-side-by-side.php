<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline1 = $glide_page_fields['headline_1'];
	$headline2 = $glide_page_fields['headline_2'];
	$content1 = $glide_page_fields['content_1'];
	$content2 = $glide_page_fields['content_2'];
	$cta1 = $glide_page_fields['cta_1'];
	$cta2 = $glide_page_fields['cta_2'];

?>
<section class="block-side-by-side">
    <div class="container-full">
      <div class="row no-gutters d-flex">
        <div class="col col-12 col-lg-6">
        	<div class="col-wrapper">
	        	<h2><?php echo $headline1;?></h2>
	        	<p class="mb-5"><?php echo $content1;?></p>
	        	<?php if($cta1):?>
		        	<a href="<?php echo $cta1['url'];?>" class="btn"><?php echo $cta1['title'];?></a>
		        <?php endif;?>
		    </div>
        </div>
        <div class="col col-12 col-lg-6">
        	<div class="col-wrapper">
	        	<h2><?php echo $headline2;?></h2>
	        	<p class="mb-5"><?php echo $content2;?></p>
	        	<?php if($cta2):?>
	        		<a href="<?php echo $cta2['url'];?>" class="btn"><?php echo $cta2['title'];?></a>
	        	<?php endif;?>
	        </div>
        </div>
      </div>
    </div>
</section>