<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$uppercase = $glide_page_fields['uppercase'];
	$cta = $glide_page_fields['cta'];
	$stack = $glide_page_fields['stack'];
?>
<section class="block-footer-cta">
	<div class="wrapper">
	    <div class="container">
	      <div class="row">
	        <div class="col-12">
	        	<div class="cta <?php echo ($stack ? 'stack' : '');?>">
	        		<div class="headline">
	        			<h2 class="<?php echo ($uppercase ? 'uppercase' : '');?>"><?php echo $headline;?></h2>
	        		</div>	
	        		<div class="link">
	        			<a href="<?php echo $cta['url'];?>" class="btn"><?php echo $cta['title'];?></a>
	        		</div>
	        	</div>
	        </div>
	      </div>
	    </div>
	</div>
</section>