<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$events = $glide_page_fields['events'];

?>
<section class="block-events">
    <div class="container">
      <div class="row">
        <div class="col-12 pb-5">
        	<h2><?= $headline;?></h2>
        </div>
        <div class="col-12">
        	<div class="row">
	        	<?php foreach ($events as $event):?>
	        		<?php $post_thumbnail = get_the_post_thumbnail_url( $event->ID, 'large' );?>
	        		<div class="col-6">
		        		<div class="event">
		        			<?php $link = get_field('link', $event->ID);?>
		        			<a href="<?=$link['url'];?>">
			        			<figure class="ratio ratio-16x9 item-thumb mb-3" style="background-image:url('<?php echo $post_thumbnail; ?>'); "></figure>
			        		</a>
		        			<p class="date"><?php echo get_field('dates', $event->ID);?></p>
			        		<h4><a href="<?=$link['url'];?>"><?php echo get_the_title( $event->ID ); ?></a></h4>
				        </div>
				    </div>
	        	<?php endforeach;?>
	        </div>
        </div>
      </div>
      </div>
    </div>
</section>