<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$ctas = $glide_page_fields['ctas'];

?>
<section class="block-three-cta">
    <div class="container">
      <div class="row">
        <div class="col-12 pb-4">
        	<h3 class="headline"><?php echo $headline;?></h3>
        </div>
        <div class="col-12">
	        <div class="row ctas">
	        	<?php foreach($ctas as $cta):?>
	        		<div class="col-12 col-lg-4 mb-3 mb-lg-0 cta-group">
	        			<div class="wrapper">
	        				<div class="headline"><h4><?php echo $cta['headline'];?></h4></div>
	        				<div class="content">
	        					<p><?php echo $cta['content'];?></p>
	        				</div>
	        				<div class="link">
		        				<a href="<?php echo $cta['cta']['url'];?>"><?php echo $cta['cta']['title'];?></a>
		        			</div>
		        			<div class="image-hover" style="background-image: url('<?php echo $cta['background_image']['url'];?>');"></div>
	        			</div>
	        		</div>
	        	<?php endforeach;?>
	        </div>
	    </div>
      </div>
    </div>
</section>