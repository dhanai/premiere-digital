<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$content = $glide_page_fields['content'];

?>
<section class="block-content-dark-bar">
	<div class="wrapper">
	    <div class="container">
	      <div class="row">
	        <div class="col-12 col-lg-7 pr-lg-5">
	        	<p><?php echo $content;?></p>
	        </div>
	      </div>
	    </div>
	</div>
</section>