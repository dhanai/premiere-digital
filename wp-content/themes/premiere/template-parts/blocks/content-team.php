<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
    $subheader = $glide_page_fields['subheader'];
	$team_members = $glide_page_fields['team_members'];

?>
<section class="block-team">
    <div class="container">
      <div class="row">
        <div class="col-12 headline">
        	<h3><?php echo $headline;?></h3>
            <p><?php echo $subheader;?></p>
        </div>
        <div class="col-12">
        	<div class="team row">
        		<?php foreach($team_members as $team):?>
        			<?php $post_thumbnail = get_the_post_thumbnail_url( $team->ID, 'large' );?>
        			<div class="col-6 col-lg-3 text-center mb-5 team-item">
	        			<a href="<?php echo get_the_permalink( $team->ID ) ?>" data-id="data-<?=$team->ID;?>">
							<figure class="ratio ratio-3x4 item-thumb mb-3" style="background-image:url('<?php echo $post_thumbnail; ?>'); "></figure>
						</a>
	        			<h4><strong><?php echo get_the_title( $team->ID ); ?></strong></h4>
	        			<p><?php echo get_field('title', $team->ID);?></p>
        			</div>

                    <div id="data-<?=$team->ID;?>" class="modal-data">
                        <div class="post-modal">
                          <div class="blog-image">
                            <figure class="ratio ratio-3x4" style="background-image: url(<?php echo $post_thumbnail; ?>); "></figure>
                          </div>
                          <div class="blog-details">
                            <h4><strong><?php echo get_the_title( $team->ID ); ?></strong></h4>
                            <p><?php echo get_field('title', $team->ID);?></p>
                            <?php echo get_field('bio', $team->ID); ?>
                          </div>
                        </div>
                    </div>

        		<?php endforeach;?>
        	</div>
        </div>
      </div>
    </div>
</section>

<script>
    jQuery('document').ready(function($){
       setTimeout(function(){
            $('.team-item a').on('click', function(e){
                e.preventDefault();
                var id = $(this).data('id');
                var data = $('#'+id ).html();
                $('.pop-modal .details').html( data );
                $('.pop-modal').addClass('show');
                $('.modal-bg').addClass('show');
            });

            $('.modal-bg').on('click', function(e){
                e.preventDefault();
                $('.pop-modal').removeClass('show');
                $('.modal-bg').removeClass('show');
                $('.pop-modal .details').html('');
            });

            $('.close-modal').on('click', function(e){
                e.preventDefault();
                $('.pop-modal').removeClass('show');
                $('.modal-bg').removeClass('show');
                $('.pop-modal .details').html('');
            });
        });
    });
</script>