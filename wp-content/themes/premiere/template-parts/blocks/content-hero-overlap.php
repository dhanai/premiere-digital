<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$content = $glide_page_fields['content'];
	$background_image = $glide_page_fields['background_image'];
?>
<section class="block-hero-overlap">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-9 content">
        	<h2><?php echo $headline;?></h2>
        	<p><?php echo $content;?></p>
        </div>
        <div class="col-12 col-lg-8 box-image">
        	<div class="ratio ratio-2x1" style="background-image: url('<?php echo $background_image['url'];?>');"></div>
        </div>
      </div>
    </div>
</section>