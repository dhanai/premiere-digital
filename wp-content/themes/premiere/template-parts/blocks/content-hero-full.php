<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$content = $glide_page_fields['content'];
	$background_image = $glide_page_fields['background_image'];

?>
<section class="block-hero-full">
    <div class="hero-wrapper">
	    <div class="container">
	      <div class="row">
	        <div class="col-12 col-lg-9">
	        	<h2><?php echo $headline;?></h2>
	        	<p><?php echo $content;?></p>
	        </div>
	      </div>
	    </div>
	    <div class="hero-image" style="background-image: url('<?php echo $background_image['url'];?>');"></div>
	</div>

    <div class="spacer">
		<div class="container">
	      <div class="row">
	        <div class="col-12 col-lg-9">
	        	<h2><?php echo $headline;?></h2>
	        	<p><?php echo $content;?></p>
	        </div>
	      </div>
	    </div>
	</div>
</section>