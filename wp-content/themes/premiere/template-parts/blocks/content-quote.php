<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$image = $glide_page_fields['image'];
	$content = $glide_page_fields['content'];

?>
<section class="block-quote">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-6 mb-5 mb-lg-0">
        	<img src="<?php echo $image['url'];?>">
        </div>
        <div class="col-12 col-lg-6 pl-lg-5 quote">
        	<?php echo $content;?>
        </div>
      </div>
    </div>
</section>