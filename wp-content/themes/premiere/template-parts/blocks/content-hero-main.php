<?php 
  global $browser, $this_post, $quantity;  
  
	$glide_page_fields = get_fields($block['id']);
	$headline = $glide_page_fields['headline'];
	$background_image = $glide_page_fields['background_image'];
	$color_tint = $glide_page_fields['color_tint'];

?>
<section class="block-hero-main">
	<div class="hero-wrapper" style="background-color: <?php echo $color_tint;?>;">
	    <div class="container">
	      <div class="row">
	        <div class="col-12">
	        	<h1><?php echo $headline;?></h1>
	        </div>
	      </div>
	    </div>
	    <div class="hero-image" style="background-image: url('<?php echo $background_image['url'];?>');"></div>
	</div>

    <div class="spacer">
		<div class="container">
	      <div class="row">
	        <div class="col-12">
	        	<h1><?php echo $headline;?></h1>
	        </div>
	      </div>
	    </div>
	</div>

</section>
