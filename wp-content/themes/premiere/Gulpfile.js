// Load plugins
const autoprefixer  = require("autoprefixer");
const browsersync   = require("browser-sync").create();
const cssnano       = require("cssnano");
const del           = require("del");
const gulp          = require("gulp");
const imagemin      = require("gulp-imagemin");
const newer         = require("gulp-newer");
const plumber       = require("gulp-plumber");
const postcss       = require("gulp-postcss");
const rename        = require("gulp-rename");
const sass          = require("gulp-sass");
const concat        = require("gulp-concat");


// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: "./"
    },
    port: 3000
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Clean assets
function clean() {
  return del(["./dist/"]);
}

// Optimize Images
function images() {
  return gulp
    .src("./assets/images/**/*")
    .pipe(newer("./dist/images"))
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
              collapseGroups: true
            }
          ]
        })
      ])
    )
    .pipe(gulp.dest("./dist/images"));
}

// CSS task
function css() {
  return gulp
    .src("./assets/styles/manifest.scss")
    .pipe(plumber())
    .pipe(sass())
    .pipe(rename('bundle.css'))
    .pipe(gulp.dest("./dist/styles/"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(postcss([autoprefixer({ browsers: ['last 2 versions', 'ie >= 9', 'android >= 4.4', 'ios >= 7']}), cssnano()]))
    .pipe(gulp.dest("./dist/styles/"));

    //TODO: add in css mapping?
    
}

// Transpile, concatenate and minify scripts
function scripts() {
  //TODO: add in some linting
  //TODO: add in mapping?
  
  return (
    gulp
      .src(["./assets/scripts/**/*"])
      .pipe(plumber())
      .pipe(gulp.dest("./dist/scripts/src"))
      .pipe(concat('bundle.js'))
      .pipe(gulp.dest("./dist/scripts/"))
  );
  
  
}


// Watch files
function watchFiles() {
  gulp.watch("./assets/styles/**/*", css);
  gulp.watch("./assets/scripts/**/*", gulp.series(scripts));
  //gulp.watch("./assets/images/**/*", images);
}

// define complex tasks
//const js = gulp.series(scripts);
const build = gulp.series(clean, gulp.parallel(css,scripts,images));
const watch = gulp.parallel(watchFiles);

// export tasks
exports.images = images;
exports.css = css;
exports.scripts = scripts;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = build;