<?php
// Blog Detail page

get_header('dark'); 
$pID = get_the_ID();
the_post();
$post_thumbnail = get_the_post_thumbnail_url( $pID, 'large' );
$title = get_field('title');
$bio = get_field('bio');
$post_width = get_field('post_width');
$post_width = $post_width ? $post_width : 'w50';

?>

<div class="default-page"><!-- PAGE IDENTIFIER TAG -->

<div class="post-container container <?php echo $post_width; ?> p-5">
	<div class="row">
		<div class="col-12 col-lg-5 blog-image">
	    	<figure class="ratio ratio-3x4 mb-4" style="background-image: url(<?php echo $post_thumbnail; ?>); "></figure>
	    </div>
	    <div class="col-12 col-lg-7 pl-lg-5">
		    <div class="cat-pill"><?=$title; ?></div>
		    <h1><?php echo get_the_title( $pID ); ?></h1>
		    <?=$bio ?>
		</div>
	</div>
</div>

<?php
get_footer();
?>


