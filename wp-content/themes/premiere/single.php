<?php
// Blog Detail page

get_header(); 
$pID = get_the_ID();
the_post();

?>

<div class="default-page"><!-- PAGE IDENTIFIER TAG -->

<!-- MASTHEAD -->

<section class="content blog-content">

    <div class="grid-container">
      
      <div class="grid-x grid-padding-x">    

        <div class="cell large-8">

        <?php
          the_content();
        ?>
  
        </div>
  
      </div>

  </div>
</section>

<?php
get_footer();
?>


