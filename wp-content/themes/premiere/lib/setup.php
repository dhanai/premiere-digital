<?php

namespace Glide\RealDeal\Setup;

/**
 * Theme setup
 */
 
 
function setup() {

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'tb_left_menu' => 'Top Bar Left',
    'tb_right_menu' => 'Top Bar Right',
    'social_menu' => 'Social',
    'footer1_menu' => 'Footer 1',
    'footer2_menu' => 'Footer 2',
    'footer3_menu' => 'Footer 3',
    
  ]);
  
  // Register custom image sizes
  add_image_size( 'masthead', 1640, 600 );
  add_image_size( 'masthead-mobile', 683, 480 );
  add_image_size( 'icon_64', 64, 64);
  add_image_size( 'icon_58', 58, 58);
  add_image_size( 'icon_32', 32, 32);
  add_image_size( 'icon_20', 20, 20);
  add_image_size( 'cta_bg', 1680, 450, 'true');
  add_image_size( 'post_preview', 367, 280, array( 'center', 'center' ));
  add_image_size( 'post_preview_list', 110, 88, array( 'center', 'center' ));
  add_image_size( 'post_preview_featured', 765, 453, array( 'center', 'center' ));
  add_image_size( 'subscribe_modal', 962, 475, array( 'center', 'center' ));
  add_image_size( 'profile_photo_small', 120, 120, array( 'center', 'center' ));
  add_image_size( 'profile_photo_large', 220, 220, array( 'center', 'center' ));

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  add_theme_support( 'align-wide' );

  # Set JPEG quality to 100%
  add_filter('jpeg_quality', function() { return 100; });
  # Time format for the_time()
  DEFINE('tFormat', 'M d, Y');


  
  DEFINE('assetDir', get_template_directory_uri() . '/dist/');
  DEFINE('ASSET_VERSION', filemtime(get_template_directory() . '/dist/styles/bundle.min.css') /* + filemtime(get_template_directory() . '/dist/js/bundle.js') */);




  // Elminiate the emoji script
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('wp_print_styles', 'print_emoji_styles');
  
  
  
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');


/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('redhatdisplay', 'fonts.googleapis.com/css?family=Red+Hat+Display:400,400i,700,700i,900,900i&display=swap', false, null);
  wp_enqueue_style('glide/cssbundle', assetDir.'styles/bundle.min.css?v=2', false, null);
  wp_enqueue_style('SwiperCss', '//cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css', false, null);
  
  wp_enqueue_script("jquery");
  wp_enqueue_script('glide/mainscripts', get_template_directory_uri().'/dist/scripts/bundle.js', ['jquery'], null, false);
  wp_enqueue_script('FontAwesome', '//kit.fontawesome.com/9226d982d5.js', ['jquery'], null, false);
  wp_enqueue_script('Swiper', '//cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js', ['jquery'], null, false);
  wp_enqueue_script('ScrollReveal', '//cdnjs.cloudflare.com/ajax/libs/scrollReveal.js/4.0.5/scrollreveal.min.js', ['jquery'], null, false);

  wp_localize_script('glide/mainscripts', 'localVars', array(
     'ajaxurl' => admin_url('admin-ajax.php')
  ));
  
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);