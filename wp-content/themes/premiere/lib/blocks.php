<?php 
//======================================================================
// GUTENBERG-ACF CONTENT BLOCKS
//======================================================================


add_action('acf/init', 'my_acf_blocks');
function my_acf_blocks() {
	// check function exists
	if( function_exists('acf_register_block') ) {
	
		acf_register_block(array(
			'name'				=> 'hero_main',
			'title'				=> __('Hero Main'),
			'description'		=> __('Hero Main'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'format-image',
			'keywords'			=> array( 'hero', 'main' ),
		));

		acf_register_block(array(
			'name'				=> 'side_by_side',
			'title'				=> __('Side by Side'),
			'description'		=> __('Side by Side'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'side' ),
		));

		acf_register_block(array(
			'name'				=> 'three_cta',
			'title'				=> __('Three CTA'),
			'description'		=> __('Three CTA'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'slides',
			'keywords'			=> array( 'three' ),
		));

		acf_register_block(array(
			'name'				=> 'carousel',
			'title'				=> __('Carousel'),
			'description'		=> __('Carousel'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'slides',
			'keywords'			=> array( 'carousel' ),
		));

		acf_register_block(array(
			'name'				=> 'quote',
			'title'				=> __('Quote'),
			'description'		=> __('Quote'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'quote' ),
		));

		acf_register_block(array(
			'name'				=> 'logo_repeater',
			'title'				=> __('Logo Repeater'),
			'description'		=> __('Logo Repeater'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'logo' ),
		));

		acf_register_block(array(
			'name'				=> 'footer_cta',
			'title'				=> __('Footer CTA'),
			'description'		=> __('Footer CTA'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'footer', 'cta' ),
		));

		acf_register_block(array(
			'name'				=> 'hero_blue',
			'title'				=> __('Hero Blue'),
			'description'		=> __('Hero Blue'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'hero', 'blue' ),
		));

		acf_register_block(array(
			'name'				=> 'content_dark_bar',
			'title'				=> __('Content Dark Bar'),
			'description'		=> __('Content Dark Bar'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'content', 'dark' ),
		));

		acf_register_block(array(
			'name'				=> 'team',
			'title'				=> __('Team'),
			'description'		=> __('Team'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'team' ),
		));

		acf_register_block(array(
			'name'				=> 'testimonial',
			'title'				=> __('Testimonial'),
			'description'		=> __('Testimonial'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'testimonial' ),
		));

		acf_register_block(array(
			'name'				=> 'hero_overlap',
			'title'				=> __('Hero Overlap'),
			'description'		=> __('Hero Overlap'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'hero', 'overlap' ),
		));

		acf_register_block(array(
			'name'				=> 'accordion',
			'title'				=> __('Accordion'),
			'description'		=> __('Accordion'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'accordion' ),
		));

		acf_register_block(array(
			'name'				=> 'hero_full',
			'title'				=> __('Hero Full'),
			'description'		=> __('Hero Full'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'hero', 'full' ),
		));

		acf_register_block(array(
			'name'				=> 'press_releases',
			'title'				=> __('Press Releases'),
			'description'		=> __('Press Releases'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'press', 'release' ),
		));

		acf_register_block(array(
			'name'				=> 'events',
			'title'				=> __('Events'),
			'description'		=> __('Events'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'events' ),
		));

		acf_register_block(array(
			'name'				=> 'contact_form',
			'title'				=> __('Contact Form'),
			'description'		=> __('Contact Form'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'glide-blocks',
			'mode'			=> 'preview',
			'icon'				=> 'media-document',
			'keywords'			=> array( 'contact', 'form' ),
		));
	}
}


function my_acf_block_render_callback( $block ) {
	$slug = str_replace('acf/', '', $block['name']);
	// include a template part from within the "template-parts/block" folder
  if( file_exists( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/blocks/content-{$slug}.php") );
	}
}

/**
 * register custom block category
 */
function my_plugin_block_categories( $categories, $post ) {

	// if ( $post->post_type !== 'post' ) {
	// 	return $categories;
	// }

	$new_category = array(array(
		'slug' => 'glide-blocks',
		'title' => __( 'Premiere Blocks', 'glide-blocks' ),
	));

	array_splice($categories, 0, 0, $new_category);
	return $categories;
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );

/**
 * Enqueue JavaScript
 */
function block_styles_enqueue_javascript() {
    wp_enqueue_script( 'block-styles-script',
        get_theme_file_uri(  'blocks.js'),
        array( 'wp-blocks')
    );
} 

add_action( 'enqueue_block_editor_assets', 'block_styles_enqueue_javascript' ); 


/**
 * Enqueue Stylesheet
 */
function block_styles_enqueue_stylesheet() {
    wp_enqueue_style( 'block-styles-stylesheet', get_theme_file_uri( '/dist/styles/blockstyles.css'));
}
//add_action( 'enqueue_block_assets', 'block_styles_enqueue_stylesheet' ); 