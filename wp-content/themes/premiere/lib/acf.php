<?php 
//======================================================================
// ACF FUNCTIONS & FIELDS
//======================================================================

if( function_exists('acf_add_options_page') ) {
  $option_page = acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title' 	=> 'Theme Options',
		'menu_slug' 	=> 'acf-options',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false,
		'position' => 2
	));
}

// Function to prevent php errors when the ACF field isn't in the field array
function snag_field($name) {
  global $glide_page_fields;
  return (isset($glide_page_fields[$name]) ? $glide_page_fields[$name] : "");
}

// Helper function that builds button from ACF link object
function build_acf_button($object, $classes="") {
  $link = "";
  $link = "<a href='".$object['url']."' target='".$object['target']."' class=' ".$classes." '>".$object['title']."</a>";
  
  return $link;
}

// Allow adding default image to ACF 
add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field');
function add_default_value_to_image_field($field) {
		acf_render_field_setting( $field, array(
			'label'			=> 'Default Image',
			'instructions'		=> 'Appears when creating a new post',
			'type'			=> 'image',
			'name'			=> 'default_value',
		));
	}


