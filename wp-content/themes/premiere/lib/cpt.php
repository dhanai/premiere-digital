<?php
//======================================================================
// CUSTOM POST TYPES
//======================================================================

function glide_cpts() {

    $labels2 = array(
		'name' 				 => __( 'Team Members', '' ),
		'singular_name' 	 => __( 'Team Member', '' ),
		'add_new'			 => __( 'Add Team Member', '' ),
		'add_new_item'		 => __( 'Add New Team Member', '' ),
		'new_item'           => __( 'New Team Member', '' ),
		'edit_item'          => __( 'Edit Team Member', '' ),
		'view_item'          => __( 'View Team Member', '' ),
		'all_items'          => __( 'All Team Members', '' ),
		'search_items'       => __( 'Search Team Members', '' ),
		'parent_item_colon'  => __( 'Parent Team Members:', '' ),
		'not_found'          => __( 'No team members found.', '' ),
		'not_found_in_trash' => __( 'No team members found in Trash.', '' ),
		'item_updated'       => __( 'Team Member Updated', '' )
	);

    $args2 = array(
      'public' => true,
      'label' => __( 'Team Members', '' ),
	  'labels' => $labels2,
      'supports' => array( 'title', 'thumbnail' )
    );
    register_post_type( 'team-members', $args2 );

    $labels3 = array(
		'name' 				 => __( 'Events', '' ),
		'singular_name' 	 => __( 'Event', '' ),
		'add_new'			 => __( 'Add Event', '' ),
		'add_new_item'		 => __( 'Add New Event', '' ),
		'new_item'           => __( 'New Event', '' ),
		'edit_item'          => __( 'Edit Event', '' ),
		'view_item'          => __( 'View Event', '' ),
		'all_items'          => __( 'All Events', '' ),
		'search_items'       => __( 'Search Event', '' ),
		'parent_item_colon'  => __( 'Parent Event:', '' ),
		'not_found'          => __( 'No events found.', '' ),
		'not_found_in_trash' => __( 'No events found in Trash.', '' ),
		'item_updated'       => __( 'Event Updated', '' )
	);

    $args3 = array(
      'public' => true,
      'label' => __( 'Events', '' ),
	  'labels' => $labels3,
      'supports' => array( 'title', 'thumbnail' )
    );
    register_post_type( 'events', $args3 );

    $labels4 = array(
		'name' 				 => __( 'Press Releases', '' ),
		'singular_name' 	 => __( 'Press Release', '' ),
		'add_new'			 => __( 'Add Press Release', '' ),
		'add_new_item'		 => __( 'Add New Press Release', '' ),
		'new_item'           => __( 'New Press Release', '' ),
		'edit_item'          => __( 'Edit Press Release', '' ),
		'view_item'          => __( 'View Press Release', '' ),
		'all_items'          => __( 'All Press Releases', '' ),
		'search_items'       => __( 'Search Press Releases', '' ),
		'parent_item_colon'  => __( 'Parent Press Release:', '' ),
		'not_found'          => __( 'No press releases found.', '' ),
		'not_found_in_trash' => __( 'No press releases found in Trash.', '' ),
		'item_updated'       => __( 'Press Release Updated', '' )
	);

    $args4 = array(
      'public' => true,
      'label' => __( 'Press Releases', '' ),
	  'labels' => $labels4,
      'supports' => array( 'title', 'thumbnail' )
    );
    register_post_type( 'press-releases', $args4 );
}

add_action( 'init', 'glide_cpts' );

?>
