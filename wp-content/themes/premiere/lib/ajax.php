<?php
/**
 * Custom Ajax Functions
 */
 
function ajax_search() {
  // Get search term from search field
  $search = sanitize_text_field( $_POST[ 'query' ] );
  
  // Set up query using search string, limit to 8 results
  $query = new WP_Query(
    array(
      'posts_per_page' => 18,
      's' => $search
    )
  );
  
  $output = '';
  
  // Run search query
  if ( $query->have_posts() ) {
    while ( $query->have_posts() ) : $query->the_post();
      
      /* Output a link to each result
         This is where the post thumbnail, excerpt, or anything else could be added */
      echo get_the_title() . "<br>" ;
    
    endwhile;        
    
    // If there is more than one page of results, add link to the full results page
    if ( $query->max_num_pages > 1 ) {
      // We use urlencode() here to handle any spaces or odd characters in the search string
      //echo '&lt;a class="see-all-results" href="' . get_site_url() . '?s=' . urlencode( $search ) . '"&gt;View all results&lt;/a&gt;';
    }
    
  } else {
    
    // There are no results, output a message
    echo '&lt;p class="no-results"&gt;No results&lt;/p&gt;';
  
  }
  
  // Reset query
  wp_reset_query();
  
  die();
}

add_action( 'wp_ajax_ajax_search', 'ajax_search' );
add_action( 'wp_ajax_nopriv_ajax_search', 'ajax_search' );


function ajax_load_more() {
  // Get search term from search field
  $cat = $_POST[ 'cat' ];
  $page = $_POST[ 'page' ];
  $off = $_POST[ 'offset' ];
  
  $offset = ($page*8)+($off*$page);
  //$cat = sanitize_text_field( $_POST[ 'cat' ] );
  
  // Set up query using search string, limit to 8 results
  $query = new WP_Query(
    array(
      'posts_per_page' => '',
      's' => '',
      'cat' => $cat,
      'offset' => $offset
    )
  );
  
  $output = '';
  
  // Run search query
  if ( $query->have_posts() ) {
    while ( $query->have_posts() ) : $query->the_post();
      
      /* Output a link to each result
         This is where the post thumbnail, excerpt, or anything else could be added */
         print "post here";
      //get_template_part( 'template-parts/post' );
    
    endwhile;        
    
    // If there is more than one page of results, add link to the full results page
    if ( $query->max_num_pages > 1 ) {
      // We use urlencode() here to handle any spaces or odd characters in the search string
      //echo '&lt;a class="see-all-results" href="' . get_site_url() . '?s=' . urlencode( $search ) . '"&gt;View all results&lt;/a&gt;';
    }
    
  } else {
    
    // There are no results, output a message
    echo '&lt;p class="no-results"&gt;No results&lt;/p&gt;';
  
  }
  
  // Reset query
  wp_reset_query();
  
  die();
}

add_action( 'wp_ajax_ajax_load_more', 'ajax_load_more' );
add_action( 'wp_ajax_nopriv_ajax_load_more', 'ajax_load_more' );
