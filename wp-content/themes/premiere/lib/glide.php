<?php
/**
 * Custom Glide Functions
 */
 
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types'); 
 
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {

    if( $args->theme_location == 'social_menu' ) {

       foreach( $items as &$item ) {
       // vars
        $icon = get_field('menu_icon', $item->ID);
        $icon_url = $icon['sizes']['icon_32'];
        
     		// append icon
     		if( $icon_url ) {
     			$item->title = "<img src=".$icon_url." alt=".$item->title.">";
     	  }
      }
    }
    
    
    if( $args->theme_location == 'category_navigation_menu' ) {

       foreach( $items as &$item ) {
         
       // vars
        $icon = get_field('menu_icon', $item->ID);
        $icon_url = $icon['sizes']['icon_58'];
        
     		// append icon
     		if( $icon_url ) {
     			$item->title = "<img src=".$icon_url." alt=".$item->title."><span>".$item->title."</span>";
     	  }
      }
    }

    return $items;
}