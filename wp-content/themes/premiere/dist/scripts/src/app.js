/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Glide = {
    // All pages
    common: {
      init: function() {
        
        // Remove empty paragraph tags
        //$("p:empty").remove();
        
        // Initialize All Foundation Elements
        jQuery(document).foundation();
        

        /* ============
         * Start Header 
         * ============ */


        $('.mobile-btn a').on('click', function(e){
          e.preventDefault();
          $('body').toggleClass('open-mobile');
        });

        var trigger = '200';

        // changed header on scroll
        $(window).scroll(function(e){
          var body = $('body').hasClass('sticky-header');
          if($(this).scrollTop() > trigger){
            if(!body){
              $('body').addClass('sticky-header');
            };
          }else{
            if(body){
              $('body').removeClass('sticky-header');
            };
          };
        });

        // changed header if refreshed towards the bottom
        var winTop = $(window).scrollTop();
        var body = $('body').hasClass('sticky-header');
        if( winTop > trigger ){
          if(!body){
            $('body').addClass('sticky-header');
          }
        }else{
          if(body){
            $('.sticky-header nav').removeClass('show');
            $('body').removeClass('sticky-header');
          };
        };

        let prevScrollpos = $(window).scrollTop();
        let currentScrollPos;
        
        $(window).scroll(function(){ 
          let currentScrollPos = $(window).scrollTop();  
          let scrollRange = currentScrollPos - prevScrollpos;
          
          if( !$('body').hasClass('open-mobile') ){
            if (prevScrollpos > currentScrollPos || currentScrollPos <= 0) {
              if(scrollRange < -30 || currentScrollPos <= 0){
                  $('.sticky-header nav').addClass('show');
              }
            } else {
              $('.sticky-header nav').removeClass('show');
            }
            prevScrollpos = currentScrollPos;
            
          }
        });
        
        /* ============
         * End Header 
         * ============ */      
        

        $('#load_more_posts').on('click',function() {
          getMorePosts($(this));
        });
        
        function getMorePosts(elem) {
    var id = elem.attr('id');
    var category = elem.attr('data-category');
    var target = elem.attr('data-target');
    var page = elem.attr('data-page');    
    var offset = elem.attr('data-offset');    
    
    $.ajax({
      type: 'POST',
      url: localVars['ajaxurl'], // ajaxurl comes from the localize_script we added to functions.php
      data: {
        action: 'ajax_load_more',
        cat: category,
        page: page,
        offset: offset,
      },
      success: function(result) {
          // `result` here is what we've specified in ajax-search.php
          $('.'+target).append(result);
          $('#'+id).attr('data-page', parseInt(page)+1);
          console.log(result);
      },
      complete: function() {
        // Whether or not results are returned, hide the loading icon once the request is complete
      }
    });
    
    

  }

      },
      finalize: function() {
        // JavaScript to be fired after the init JS
      }
    },
    // Home page
    home: {
      init: function() {
        
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // Single Locations
    category: {
      init: function() {

      }
    },
    single_post: {
      init: function() {

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Glide;
      funcname = funcname === undefined ? "init" : funcname;
      fire = func !== "";
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === "function";

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire("common");

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, "_").split(/\s+/), function(
        i,
        classnm
      ) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, "finalize");
      });

      // Fire common finalize JS
      UTIL.fire("common", "finalize");
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);
})(jQuery); // Fully reference jQuery after this point.
