<?php
/**
 * Real Deal includes
 *
 * Please note that missing files will produce a fatal error.
 */

$rd_includes = [
    'lib/setup.php',        // Theme setup
    'lib/extras.php',       // Custom functions for this project
    'lib/acf.php',          // ACF Functions
    'lib/blocks.php',          // Content Blocks
    'lib/cpt.php',          // CPT Setups   
    'lib/glide.php',        // Glide custom functions
    'lib/browsers.php',     // Glide browsers
    'lib/ajax.php',     // Ajax Functions

];
foreach ($rd_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }
  require_once $filepath;
}
unset($file, $filepath);
global $browser;
$browser = new Browsersphp\Browsers();